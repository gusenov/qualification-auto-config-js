function checkBox(parent, isChecked) {
 parent.find('.ez-checkbox').each(function (index, value) {
  $(this).attr('class', 'ez-checkbox' + (isChecked ? ' ez-checked' : ''));
  $(this).find(":input[type='checkbox']")
         .eq(0)
         .prop('checked', isChecked);
 });
}

function isBoxChecked(parent) {
 return parent.find('.ez-checkbox')
              .find(":input[type='checkbox']")
              .eq(0)
              .is(':checked');
}

function processList(blockClass, isEnabled, exclusions) {

 $(blockClass).find('.custom-checkbox')
              .find('.type-name')
              .each(function (index, value) {
  
  var checkboxTitle = $.trim($(this).text()),
      isChecked = isEnabled;
  
  if (exclusions.indexOf(checkboxTitle) > -1) {
   isChecked = !isEnabled;
  }

  checkBox($(this), isChecked)
 
 });
}

function processTree(nodeName, isEnabled, inclusions, exclusions) {
 $('.w-qualification-work-subjects').find('.wrapper2.source.sac-root-list')
                                    .find('.node[data-name="' + nodeName + '"]')
                                    .each(function (index, value) {
  $(this).find('.node').each(function (index, value) {
   
   var checkboxTitle = $(this).attr("data-name"),
       isChecked = isEnabled;
   
   // Если список включений непустой, то руководствуемся им.
   if (inclusions.length > 0) {
    // Если checkbox-а нет в списке включений, то даже не рассматриваем его, а сразу выходим.
    if (inclusions.indexOf(checkboxTitle) < 0) {
     return;
    }
   }

   if (exclusions.indexOf(checkboxTitle) > -1) {
    isChecked = !isEnabled;
   }
   
   if (isChecked != isBoxChecked($(this))) {
    $(this).find(":input[type='checkbox']").eq(0).click();
   }
  });
 });
}

function saveSettings() {
 $('.w-button.type2.submit').click();
}

function work() {
 processList('.w-qualification-work-types', true, ['Бизнес-план', 'Бизнес-план для бизнеса']);
 processList('.w-qualification-softwares-types', true, []);
 processTree('информатика', true, [], []);
 processTree('программирование', true, [], []);
 processTree('языки', true, ['английский начальный - средний', 'английский продвинутый'], []);
 saveSettings();
}

function doNotWork() {
 processList('.w-qualification-work-types', false, ['Другой']);
 processList('.w-qualification-softwares-types', false, []);
 processTree('информатика', false, [], 'информатика');
 processTree('программирование', false, [], []);
 processTree('языки', false, [], []);
 saveSettings();
}

//work();
//doNotWork();

